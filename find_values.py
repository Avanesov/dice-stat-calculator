def FindValues(dictionary, my_keys=''):

    for key, value in dictionary.items():

        current_key = my_keys + '[' + key + ']'

        if type(value) is dict:

            FindValues(value, current_key)

        else:

            result = 'sample_dictionary' + current_key  + ' = ' + value

            print(result)

FindValues(sample_dictionary)
