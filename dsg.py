#! /usr/bin/env python3
import sys, getopt, json

## Start defining functions
def print_help():
    '''Prints the help file.
    '''
    print('''           Dice Stat Generator
    Use: dsg.py -d 3  -w 6 -k 3 -f csv

    Arguments:                  Description:
    -h, --help                  Print this help
    -d, --dice                  The number of dice (default: 3)
    -k, --keep-high             Keep the n highest dice rolled (default: all)
    -l, --keep-low              Keep the n lowest dice rolled
    -w, --high_die              The highest roll per die (default: 6)
    -s, --low_die               The lowest roll per die (default: 1)
    -c. --command               The main command to run
    -v, --verbose               Execute with verbose output
    -f, --format                Output to format (csv, raw, default)
    --debug                     Set debug level (1, 2, 3)
                                    1: Basic debugging info
                                    2: Much more debugging info
                                    3: Prints debugging messages contained
                                        within loops. Can potentially output
                                        gigabytes of information. (Default: 0)
    --print_rolls               Will output all possible dice roll combinations
    --no_headers                Will not print headers (Default: False)
    --no_roll_totals            Will not print the roll totals on the left column
                                    (Default: False)
    --no_percent                Do not display percentage formatted roll chance
                                    on default stat printout. (Default: False)
    '''
    )

def inc_rolls(list, index, max, min, count):
    '''Arguments: List of dice, the starting index, max die roll, min die roll,
    cycle count (initially 0)
    This function takes the arguments and increments the specified index in the
    list by 1. If the index is already at max it is set to min and the function
    is called recursivly on the next index.
    '''
    debug("inc_rolls: count:", count, 3)
    debug("inc_rolls: Received list:", list, 3)
    debug("inc_rolls: Received index:", index, 3)
    debug("inc_rolls: Received max:", max, 3)
    count += 1
    if index <= len(list) - 1:
        if list[index] < max:
            list[index] += 1
            debug("inc_rolls: Less than max, returning list:", list, 3)
        elif list[index] == max:
            list[index] = min
            index += 1
            list = inc_rolls(list, index, max, min, count)
            debug("inc_rolls: else, returning list:", list, 3)
    else:
        debug("inc_rolls: Index not <= len(list), returning list:", list, 3)
    return list

def debug(message, var, level):
    '''Args: A debug message, a variable if needed, the debug level (1-normal,
    2-advanced, 3-looping or recursive messages)
    A function to add debug messages to the code.
    '''
    if master["options"]["debug"] >= 1 and master["options"]["debug"] >= level:
        #print("option is:", master["options"]["debug"])
        #print ("level is:", level)
        print("[ Debug ]",[level], message, var)

def print_error(message, var):
    '''Args: A message, a variable if needed.
    A function to add error messages to the code.
    '''
    print("[ Error ]", message, var)

def keep_n(num, weight, mylist):
    '''Args: Number of dice to keep, high or low, the list of dice rolls.
    This function keeps the highest or lowest of n dice from the list provided.
    '''
    if weight == "high":
        keeping = []
        keeping = sorted(mylist, reverse=True)[:num]
        debug("keep_n: weight= high, num=", num, 3)
        debug("keep_n: weight= high, mylist= ", mylist, 3)
        debug("keep_n: weight= high, keeping=", keeping, 3)
        return keeping
    elif weight == "low":
        keeping = []
        keeping = sorted(mylist)[:num]
        debug("keep_n: weight= low, num=", num, 3)
        debug("keep_n: weight= high, mylist= ", mylist, 3)
        debug("keep_n: weight= high, keeping=", keeping, 3)
        return keeping

def print_stats(print_format):
    '''Args: the print format (default, csv, raw)
    Prints the final statics with basic formatting.
    '''
    header = ["Roll", "Totals", "% per Roll", "# or Higher", "# or Lower"]
    if print_format in ("default"):
        print('      '.join(header))
        for i in sorted(master["rpt"].keys()):
            space1 = 10 - len(str(i))
            space2 = 20 - (len(str(i)) + len(str(master["rpt"][i]))) - space1
            print(i,
            '\t' * 1, master["rpt"][i], ' ' * (12 - len(str(master["rpt"][i]))),
            '\t' * 1, format(master["per_tot"][i] * 100, '.2f'), "%",
            '\t' * 1, format(master["per_high"][i] * 100, '.2f'), "%",
            '\t' * 1, format(master["per_low"][i] * 100, '.2f'), "%")
    elif print_format in ("raw"):
        for i in sorted(master["rpt"].keys()):
            print(i,
            master["rpt"][i],
            master["per_tot"][i],
            master["per_high"][i],
            master["per_low"][i])
    elif print_format in ("csv"):
        print(','.join(header))
        for i in sorted(master["rpt"].keys()):
            print(i,
            master["rpt"][i],
            master["per_tot"][i],
            master["per_high"][i],
            master["per_low"][i], sep = ',')
    elif print_format in ("json"):
        pass

def generate_stats(dict):
    # Set total number of combinations
    num = dict['options']['sides'] ** dict['options']['dice']
    dict.update({"total_combinations":num})
    #Generate initial dice list
    set_low = dict["options"]['low_die']
    mylist = [set_low] * dict["options"]['dice']
    # Initialize total occurances of each possible roll total.
    for i in range(dict["options"]['keep'] * dict["options"]['low_die'], dict["options"]['keep'] * dict["options"]['high_die'] + 1):
        dict["stats"].update({i:{"num_of_totals":0}})
    looping = "True"
    while looping == "True":
        bestn = keep_n(dict["options"]["keep"], dict["options"]["hl"], mylist)
        roll_total = sum(mylist)
        bestn_total = sum(bestn)
        # print("list:", mylist)
        # print("bestn:", bestn)
        # print("roll total:", roll_total)
        # print("bestn total:", bestn_total)
        if dict["options"]["print_rolls"] == "true":
            print(mylist)

        if roll_total < dict["options"]['dice'] * dict["options"]['high_die']:
            mylist = inc_rolls(mylist, 0, dict["options"]['high_die'], dict["options"]['low_die'], 0)
        else:
            mylist = inc_rolls(mylist, 0, dict["options"]['high_die'], dict["options"]['low_die'], 0)
            looping = "False"
        #print("updated results dict,", bestn_total,":", dict["stats"][bestn_total]["num_of_totals"])
        dict["stats"][bestn_total]["num_of_totals"] += 1

    # Generate the chance to roll each total.
    for i in dict["stats"].keys():
        total = dict["stats"][i]["num_of_totals"] / dict["total_combinations"]
        dict["stats"][i].update({"chance":total})
    # Generate chance to roll total or higher
    total = 0
    previous = 0
    for i in list(reversed(sorted(dict["stats"].keys()))):
        total = dict["stats"][i]["chance"] + previous
        dict["stats"][i].update({"chance+":total})
        previous = total
    # Generate chance to roll total or lower
    total = 0
    previous = 0
    for i in sorted(dict["stats"].keys()):
        total = dict["stats"][i]["chance"] + previous
        dict["stats"][i].update({"chance-":total})
        previous = total
    return dict

def get_args(argv):
    debug("get_args: Starting function", "", 1)
    try:
        opts, args = getopt.getopt(argv,"hs:d:k:l:c:f:v:w:",["format=",
        "print-rolls", "command=", "help", "dice=", "keep-high=",
        "keep-low=", "debug=", "high-die=", "low-die=", "verbose=", "no-headers",
        "no-roll-totals", "no-percent"])
    except getopt.GetoptError:
        print_error('You entered bad options!',"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_help()
            sys.exit()
        elif opt in ("-w", "--high-die"):
            master["options"]["high_die"] = int(arg)
        elif opt in ("-s", "--low-die"):
            master["options"]["low_die"] = int(arg)
        elif opt in ("-d", "--dice"):
            master["options"]["dice"] = int(arg)
        elif opt in ("-k", "--keep-high"):
            master["options"]["keep"] = int(arg)
            master["options"]["hl"] = "high"
        elif opt in ("-l", "--keep-low"):
            master["options"]["keep"] = int(arg)
            master["options"]["hl"] = "low"
        elif opt in ("--debug"):
            master["options"]["debug"] = int(arg)
        elif opt in ("-c", "--command"):
            master["options"]["command"] = (arg)
        elif opt in ("--print-rolls"):
            master["options"]["print_rolls"] = "True"
        elif opt in ("-f", "--format"):
            master["options"]["format"] = (arg)
        elif opt in ("--no-headers"):
            master["options"]["no_headers"] = "True"
        elif opt in ("--no-roll-totals"):
            master["options"]["no_roll_totals"] = "True"
        elif opt in ("--no-percent"):
            master["options"]["percents"] = "False"

def print_stats_default(dict):
    if dict["options"]["no_headers"] == "False":
        if dict["options"]["no_roll_totals"] == "False":
            print(dict["headers"]["roll_total"], end=' ' * (24 - len(dict["headers"]["roll_total"])))
        print(dict["headers"]["num_of_totals"], end=' ' * (24 - len(dict["headers"]["num_of_totals"])))
        print(dict["headers"]["chance"], end=' ' * (24 - len(dict["headers"]["chance"])))
        print(dict["headers"]["chance_or_higher"], end=' ' * (24 - len(dict["headers"]["chance_or_higher"])))
        print(dict["headers"]["chance_or_lower"],)
    for i in sorted(dict["stats"].keys()):
        if dict["options"]["no_roll_totals"] == "False":
            print(i, end=' ' * (24 - len(str(i))))
        if dict["options"]["percents"] == "True":
            var1 = dict["stats"][i]["num_of_totals"]
            var2 = format(dict["stats"][i]["chance"] * 100, '.2f')
            var3 = format(dict["stats"][i]["chance+"] * 100, '.2f')
            var4 = format(dict["stats"][i]["chance-"] * 100, '.2f')
            var5 = "%"
        else:
            var1 = dict["stats"][i]["num_of_totals"]
            var2 = dict["stats"][i]["chance"]
            var3 = dict["stats"][i]["chance+"]
            var4 = dict["stats"][i]["chance-"]
            var5 = ""
        print(var1, end=' ' * (24 - len(str(var1))))
        print(var2, var5, end=' ' * (23 - len(str(var2) + str(var5))))
        print(var3, var5, end=' ' * (23 - len(str(var3) + str(var5))))
        print(var4, var5)

def print_stats_csv(dict):
    if dict["options"]["no_headers"] == "False":
        if dict["options"]["no_roll_totals"] == "False":
            print(dict["headers"]["roll_total"], end=',')
        print(dict["headers"]["num_of_totals"], end=',')
        print(dict["headers"]["chance"], end=',')
        print(dict["headers"]["chance_or_higher"], end=',')
        print(dict["headers"]["chance_or_lower"],)
    for i in sorted(dict["stats"].keys()):
        if dict["options"]["no_roll_totals"] == "False":
            print(i, end=',')
        if dict["options"]["percents"] == "True":
            var1 = dict["stats"][i]["num_of_totals"]
            var2 = format(dict["stats"][i]["chance"] * 100, '.2f')
            var3 = format(dict["stats"][i]["chance+"] * 100, '.2f')
            var4 = format(dict["stats"][i]["chance-"] * 100, '.2f')
            var5 = "%"
        else:
            var1 = dict["stats"][i]["num_of_totals"]
            var2 = dict["stats"][i]["chance"]
            var3 = dict["stats"][i]["chance+"]
            var4 = dict["stats"][i]["chance-"]
            var5 = ""
        print(var1, sep='', end=',')
        print(var2, var5, sep='', end=',')
        print(var3, var5, sep='', end=',')
        print(var4, var5, sep='')

def pp_dict(m, *args):
    indent = args[0]
    for i in sorted(m.keys()):
        if isinstance(m[i], dict):
            pass
        else:
            print('\t' * indent, i,': ', m[i], sep="")
    for i in sorted(m.keys()):
        if isinstance(m[i], dict):
            if i != "stats":
                print('\t' * indent, i, ':', sep="")
                d = m[i]
                pp_dict(d, args[0] + 1)

def print_dict(dict):
    for i in sorted(dict.keys()):
        print(i,' ' * (18 - len(i)), "= ",dict[i], sep="")

def print_stats_json(dict):
    print(json.dumps(dict, indent=4, sort_keys=True))

def print_stats(dict):
    if master["options"]["format"] == "default":
        print_stats_default(dict)
    elif master["options"]["format"] == "raw":
        print(dict["stats"])
    elif master["options"]["format"] == "csv":
        print_stats_csv(dict)
    elif master["options"]["format"] == "json":
        print_stats_json(dict["stats"])

## Initialize variables and maps and get the default options defined.
master = {"stats":{}, "options":{"debug":0, "print_rolls":"False", "command":"default",
 "hl":"high", "dice":3, "sides":6, "format":"default", "low_die":1, "high_die":6,
 "headers":"True", "roll_num":"True", "no_headers":"False", "no_roll_totals":"False",
 "percents":"True"}, "headers":{"roll_total":"Roll Total", "num_of_totals":"Frequency",
 "chance":"Chance", "chance_or_higher":"Chance+", "chance_or_lower":"Chance-"}}

if __name__ == "__main__":
    # Get initial arguments and add them to the dictionary
    get_args(sys.argv[1:])
    # if "keep" isnt defined set the default value
    master["options"].setdefault("keep", master["options"]["dice"])
    #Set the number of sides per die
    master["options"]["sides"] = master["options"]["high_die"] - master["options"]["low_die"] + 1

    debug("All options are: ", master["options"], 1)
    total = master["options"]["sides"] ** master["options"]["dice"]
    debug("Total rolls are: ", total, 1)

    ## Begin main logic and control flow.
    if master["options"]["command"] == "default":
        debug("Main: Command is \"default\":","",1)
        generate_stats(master)
        print_stats(master)
    elif master["options"]["command"] == "test":
        generate_stats(master)
        print_stats_json(master[stats])
    else:
        print_error("<-c, --command> Command not recognized!: ", master["options"]["command"])
        sys.exit()
