#! /usr/bin/env python3
import sys, json, os, pathlib, time

## Start defining functions
def inc_rolls(list, index, max, min, count):
    '''Arguments: List of dice, the starting index, max die roll, min die roll,
    cycle count (initially 0)
    This function takes the arguments and increments the specified index in the
    list by 1. If the index is already at max it is set to min and the function
    is called recursivly on the next index.
    '''
    count += 1
    if index <= len(list) - 1:
        if list[index] < max:
            list[index] += 1
        elif list[index] == max:
            list[index] = min
            index += 1
            list = inc_rolls(list, index, max, min, count)
    else:
        return list

def generate_stats(diel, kplist, numl):
    master = {}
    for die in diel:
        for num in numl:
            print('\n', "Starting on:", str(num) + 'd' + str(die))
            localtime = time.asctime( time.localtime(time.time()) )
            starticks = time.time()
            print ("[ Start Time ]", localtime)
            rollist = [1] * num
            looping = True
            total_combinations = die ** num
            while looping:
                if sum(rollist) == num * die:
                    looping = False
                for kp in kplist:
                    if kp <= num:
                        keephigh = sum(sorted(rollist, reverse=True)[:kp])
                        keeplow = sum(sorted(rollist)[:kp])
                        nameh = str(num) + "d" + str(die) + "-k" + str(kp) + "h"
                        namel = str(num) + "d" + str(die) + "-k" + str(kp) + "l"
                        if str(nameh) not in master:
                            master[str(nameh)] = {}
                        if str(keephigh) not in master[str(nameh)]:
                            master[str(nameh)][str(keephigh)] = {}
                        if "num_of_totals" not in master[str(nameh)][str(keephigh)]:
                            master[str(nameh)][str(keephigh)]["num_of_totals"] = 0
                        master[str(nameh)][str(keephigh)]["num_of_totals"] += 1
                        if looping == False:
                            calc_other_stats(die, num, master[str(nameh)])
                            print_to_file(str(nameh), master[str(nameh)])
                        if kp != num:
                            if str(namel) not in master:
                                master[str(namel)] = {}
                            if str(keeplow) not in master[str(namel)]:
                                master[str(namel)][str(keeplow)] = {}
                            if "num_of_totals" not in master[str(namel)][str(keeplow)]:
                                master[str(namel)][str(keeplow)]["num_of_totals"] = 0
                            master[str(namel)][str(keeplow)]["num_of_totals"] += 1
                            if looping == False:
                                calc_other_stats(die, num, master[str(namel)])
                                print_to_file(str(namel), master[str(namel)])
                inc_rolls(rollist, 0, die, 1, 0)
            localtime = time.asctime( time.localtime(time.time()) )
            finishticks = time.time()
            print ("[ Finish Time ]", localtime)
            print("Completed", total_combinations, "combinations", "in", format(finishticks - starticks, '.3f'), "seconds")
            print_to_file("master", master)
    return master

def print_to_file(fname, dictionary):
    path = './pregen/json/'
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    f = open(path + r'/' + fname + '.json', 'w')
    f.write(json.dumps(dictionary, indent=4, sort_keys=True))
    f.close()

def calc_other_stats(die, num, dictionary):
    max_comb = die ** num
    for key in dictionary:
        num_tots = dictionary[str(key)]["num_of_totals"]
        dictionary[str(key)]["chance"] = num_tots / max_comb
    # Generate chance to roll total or higher
    total = 0
    previous = 0
    for i in list(reversed(sorted(dictionary.keys()))):
        total = dictionary[i]["chance"] + previous
        dictionary[i].update({"chance+":total})
        previous = total
    # Generate chance to roll total or lower
    total = 0
    previous = 0
    for i in sorted(dictionary.keys()):
        total = dictionary[i]["chance"] + previous
        dictionary[i].update({"chance-":total})
        previous = total

def print_stats_json(dictionary):
    print(json.dumps(dictionary, indent=4, sort_keys=True))
## Initialize variables and maps and get the default options defined.
dtypes = [2, 3, 4, 6, 8, 10, 12, 16, 20]
dkeep = list(range(1, 11))
dnum = list(range(1, 11))

if __name__ == "__main__":
    generate_stats(dtypes, dkeep, dnum)
    #print_stats_json(generate_stats(dtypes, dkeep, dnum))
