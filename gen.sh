#! /usr/bin/env bash
start_time=(date)
#num_sides=$1
count=0
for s in 2 4 6 8 10 12 16 20; do
    for n in {1..10}; do
        for (( k=1; k<=$n; k++ )); do
            mypath="pregen/json"
            mkdir -p ./$mypath
            myfile=$mypath/$n"d"$s"-k"$k"h".json
            count=$((count + 1))
            echo ""
            echo "[ Date ] $(date)"
            echo "count: $count, die: $s, number: $n, keep: $k"h", file: $myfile"
            if [ -s "./$myfile" ];
            then
                echo "./$myfile already exists!"
            else
                echo "Working on: ./$myfile"
                ./dsg.py --dice $n --high-die $s --keep-high $k --format json > ./$myfile
            fi
            echo "Finished json number $count at $(date)"
            if [ $k -ne $n ]
            then
                myfile=$mypath/$n"d"$s"-k"$k"l".json
                count=$((count + 1))
                echo ""
                echo "[ Date ] $(date)"
                echo "count: $count, die: $s, number: $n, keep: $k"l", file: $myfile"
                if [ -s "./$myfile" ];
                then
                    echo "./$myfile already exists!"
                else
                    echo "Working on: ./$myfile"
                    ./dsg.py --dice $n --high-die $s --keep-low $k --format json > ./$myfile
                fi
                echo "Finished json number $count at $(date)"
            fi
        done
    done
done

echo "Started at $start_time"
echo "Finished at $(date)"
