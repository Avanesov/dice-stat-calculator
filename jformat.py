#! /usr/bin/env python3
import sys, json, os, pathlib, time, glob

def print_stats_json(dict):
    print(json.dumps(dict, indent=4, sort_keys=True))

def build_stat_block(dn, dt):
    path = './pregen/json/'
    dietype = 'd' + str(dt)
    diename = str(dn) + "d" + str(dt)
    stat_block = {}
    sname = path + str(diename) + '-k' + '*' + 'l.json'
    files = glob.glob(sname)
    # for num and die "ie. 3d20" run against all keep options
    for kp in sorted(kplist, reverse=True):
        if kp <= dn:
            filename = str(diename) + '-k' + str(kp) + 'l.json'
            fullpath = path + filename
            if fullpath in files:
                print("File found:", fullpath)
                js = open(fullpath).read()
                data = json.loads(js)
                if str(diename) not in stat_block:
                    stat_block[str(diename)] = {}
                    stat_block[str(diename)][str(0)] = []
                stat_block[str(diename)][str(0)].append((str(diename) + " k" + str(kp) + "l " + "chance"))
                stat_block[str(diename)][str(0)].append((str(diename) + " k" + str(kp) + "l " + "chance+"))
                stat_block[str(diename)][str(0)].append((str(diename) + " k" + str(kp) + "l " + "chance-"))
                for key in data:
                    print("keeping:", kp, 'of', str(diename), 'for file', fullpath, "Data key:", key)
                    if str(key) not in stat_block[str(diename)]:
                        stat_block[str(diename)][str(key)] = []
                    stat_block[str(diename)][str(key)].append(data[str(key)]["chance"])
                    stat_block[str(diename)][str(key)].append(data[str(key)]["chance+"])
                    stat_block[str(diename)][str(key)].append(data[str(key)]["chance-"])
            # for i in list(range(0, 401)):
            #     for key in sorted(stat_block[str(diename)]):
            #         if  str(key) == str(i):
            #             print(key, ':', stat_block[str(diename)][key])
                print_to_file("./pregen/stats/json/", diename, stat_block[str(diename)])
    return stat_block


def build_master():
    for x in numdie:
        for y in dielist:
            stat_block = build_stat_block(x, y)
    print_to_file("./pregen/stats/json/", "master_stat_block", stat_block)


def print_to_file(fpath, fname, dictionary):
    pathlib.Path(fpath).mkdir(parents=True, exist_ok=True)
    f = open(fpath + r'/' + fname + '.json', 'w')
    f.write(json.dumps(dictionary, indent=4, sort_keys=True))
    f.close()
## Initialize variables and maps and get the default options defined.
master = {}
dielist = [2, 3, 4, 6, 8, 10, 12, 16, 20]
numdie = list(range(1, 21))
kplist = list(range(1, 21))

if __name__ == "__main__":
    build_master()
    #build_stat_block(6, 3)
    #print_stats_json(master)
